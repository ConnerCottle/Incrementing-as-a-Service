"""
test version of iaas module
"""

from iaas import __version__


def test_version():
    """
    test version of __init__ module
    """
    assert __version__ == "0.1.0"
