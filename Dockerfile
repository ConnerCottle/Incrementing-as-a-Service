FROM python:3.9.1
RUN pip install poetry 
RUN poetry config virtualenvs.create false

COPY pyproject.toml .
COPY poetry.lock .

RUN poetry install

COPY iaas ./iaas
COPY run.sh .
CMD "./run.sh"
