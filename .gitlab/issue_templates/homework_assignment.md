(Summarize the assignment concisely)

## Background

(Describe the assignment)

## Requirements

- [ ] Task 1
- [ ] Task 2
- [ ] Task 3

## Bonus

/estimate <estimate>
/label ~enhancement
/assign @ConnerCottle
