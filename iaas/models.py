"models using is iaas.database.py for tables"
# pylint: disable=no-name-in-module
# pylint: disable=no-self-argument
# pylint: disable=too-few-public-methods
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import PrimaryKeyConstraint

from .database import Base


class RoomORM(Base):
    """
    table for data in a Room
    """

    __tablename__ = "Room"

    id = Column("room_id", Integer, primary_key=True)
    room_name = Column(String(50), index=True)
    max_players = Column(Integer)
    room_password = Column(String)
    counter = relationship("CounterORM", backref="room", uselist=False)
    users = relationship(
        "UserORM", secondary=lambda: RoomUserORM.__table__, back_populates="rooms"
    )


class CounterORM(Base):
    """
    table for data in a Counter
    """

    __tablename__ = "Counter"

    id = Column("counter_id", Integer, primary_key=True)
    goal_value = Column(Integer)
    current_count = Column(Integer)
    most_recent_updater = Column(String)
    room_id = Column(
        Integer, ForeignKey(RoomORM.id), unique=True, nullable=False, index=True
    )


class UserORM(Base):
    """
    table for data in a User
    """

    __tablename__ = "User"

    id = Column("User_id", Integer, primary_key=True)
    username = Column(String(50), unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)
    times_incremented = Column(Integer, default=0, nullable=False)
    rooms = relationship(
        RoomORM, secondary=lambda: RoomUserORM.__table__, back_populates="users"
    )


class RoomUserORM(Base):
    """
    relational table for Room and User
    """

    __tablename__ = "RoomUser"

    user_id = Column(Integer, ForeignKey(UserORM.id), nullable=False, index=True)
    room_id = Column(Integer, ForeignKey(RoomORM.id), nullable=False, index=True)

    __tableargs__ = (PrimaryKeyConstraint(user_id, room_id),)
