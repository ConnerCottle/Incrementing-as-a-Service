#! /usr/bin/env bash
image_path=${1:?give argument pls}
docker build -t $image_path .
if [ "$PUSH_IMAGE" == "TRUE" ]; then
    docker push $image_path
fi
