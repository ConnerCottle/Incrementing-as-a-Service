"""Repository of functions that are used in the services business logic for API endpoints"""
# pylint: disable=missing-function-docstring
from datetime import datetime
from sqlalchemy.orm import Session


from .hashing import Hasher
from .models import RoomORM, CounterORM, UserORM, RoomUserORM
from .schemas import RoomCreateModel, UserCreateModel

now = datetime.now()

current_time = now.strftime("%H:%M:%S")


def get_room(database: Session, room_id: int):
    return database.query(RoomORM).filter(RoomORM.id == room_id).one()


def get_room_by_room_name(database: Session, room_name: str):
    return database.query(RoomORM).filter(RoomORM.room_name == room_name).one()


def get_rooms(database: Session, skip: int = 0, limit: int = 100):
    return database.query(RoomORM).offset(skip).limit(limit).all()


def create_room(database: Session, room: RoomCreateModel):
    database_room = RoomORM(
        room_name=room.room_name,
        max_players=room.max_players,
        room_password=room.room_password,
    )
    database.add(database_room)
    database.commit()
    database.refresh(database_room)
    return database_room


def get_counter(database: Session, counter_id: int):
    return database.query(CounterORM).filter(CounterORM.id == counter_id).one()


def update_counter(database: Session, counter_id: int, new_current_count: int):
    counter = get_counter(database, counter_id)
    counter.current_count = new_current_count


def update_most_recent_updater(
    database: Session, counter_id: int, new_most_recent_updater: str
):
    counter = get_counter(database, counter_id)
    counter.most_recent_updater = new_most_recent_updater


def get_counters(database: Session, skip: int = 0, limit: int = 100):
    return database.query(CounterORM).offset(skip).limit(limit).all()


def create_room_counter(
    database: Session, goal_value: int, room_id: int, current_count: int
):
    database_counter = CounterORM(
        goal_value=goal_value, current_count=current_count, room_id=room_id
    )
    database.add(database_counter)
    database.commit()
    database.refresh(database_counter)
    return database_counter


def get_user(database: Session, user_id: int):
    return database.query(UserORM).filter(UserORM.id == user_id).one()


def update_times_incremented(
    database: Session, user_id: int, new_times_incremented: int
):
    user = get_user(database, user_id)
    user.times_incremented = new_times_incremented


def get_user_by_username(database: Session, username: str):
    return database.query(UserORM).filter(UserORM.username == username).one()


def get_users(database: Session, skip: int = 0, limit: int = 100):
    return database.query(UserORM).offset(skip).limit(limit).all()


def create_user(database: Session, user: UserCreateModel):
    hashed_password = Hasher.get_password_hash(user.password)
    database_user = UserORM(username=user.username, hashed_password=hashed_password)
    database.add(database_user)
    database.commit()
    database.refresh(database_user)
    return database_user


def create_room_user(database: Session, user_id: int, room_id: int):
    database_user = database.query(UserORM).filter(UserORM.id == user_id).one()
    database_room = database.query(RoomORM).filter(RoomORM.id == room_id).one()

    database_room_user = RoomUserORM(user_id=database_user.id, room_id=database_room.id)
    database.add(database_room_user)
    database.commit()
    database.refresh(database_room_user)
    return database_room_user
