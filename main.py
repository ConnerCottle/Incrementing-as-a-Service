from iaas import services

counter_1 = services.Counter(15)
room = services.Room("Boyz", 8, ["Jordan", "Conner"], counter_1, "password")

# print(room)
room.add_player("Blake", "password")
# print(room)
# room.add_player("Brandon", "pazzword") creates error since password is incorrect

room.increment("Blake")
# print(room)
# room.increment("Brandon") creates an error since there is no player with that name in the room

print(room.add_player("Blake2", "password"))
print(room.increment("Blake"))
