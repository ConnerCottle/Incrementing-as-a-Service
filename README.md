# Incrementing-as-a-Service
Why count up locally inside your program when you can add an external dependency to do it for you?

This API allows you to create games of "count up" where you and your friends can take turns incrementing a number.

A game of "count up" plays out like this:

* Player 1 creates the game and invites Player 2
* Counter starts at 0
* Player 1 POSTs to the game
* Counter is now at 1
* Player 2 POSTs to the game
* Counter is now at 2

## Rules
* Players cannot increment the number twice in a row, they must take turns
* Numbers only increase by one at a time