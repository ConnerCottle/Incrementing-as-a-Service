#! /usr/bin/env bash
python << EOF
from iaas.database import get_engine
from iaas.models import Base
Base.metadata.create_all(bind=get_engine())
EOF


uvicorn --host "0.0.0.0" iaas.app:app
