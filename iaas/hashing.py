"""This is a module for hashing.
Sets up a Hasher class that can verify and get password hashes and return them
for storing in a database"""

from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class Hasher:
    """
    Class allowing for abstraction on the hashing static methods
    """

    @staticmethod
    def verify_password(plain_password, hashed_password):
        """
        Verifies a password hash
        """
        return pwd_context.verify(plain_password, hashed_password)

    @staticmethod
    def get_password_hash(password):
        """
        Returns a hash for any given password
        """
        return pwd_context.hash(password)
